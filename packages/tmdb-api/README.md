# Typescript axios client for TMDB

## Credit

The `oas.json` file was copied from [tmdb-client-rs](https://github.com/bcourtine/tmdb-client-rs) and slightly modified to make it work with the typescript axios client generator from the [OpenAPI Generator](https://openapi-generator.tech/) project.
