import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
// import { store } from '@/store'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search.vue'),
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import('../views/Settings.vue'),
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
  },
  {
    path: '/movieList',
    name: 'MovieList',
    component: () => import('../views/MovieList.vue'),
  },
  {
    path: '/tvList',
    name: 'TvList',
    component: () => import('../views/TvList.vue'),
  },
  {
    path: '/movie/:movieId',
    name: 'Movie',
    component: () => import('../views/Movie.vue'),
    props(route) {
      const movieId = typeof route.params.movieId === 'string' ? parseInt(route.params.movieId, 10) : undefined

      return {
        movieId,
      }
    },
  },
  {
    path: '/tv/:tvId',
    name: 'Tv',
    component: () => import('../views/Tv.vue'),
    props(route) {
      const tvId = typeof route.params.tvId === 'string' ? parseInt(route.params.tvId, 10) : undefined

      return {
        tvId,
      }
    },
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'Not Found',
    component: () => import('../views/NotFound.vue'),
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

router.beforeEach(async (to, from, next) => {
  // @ts-ignore
  // await store.restored
  next()
})

export default router
