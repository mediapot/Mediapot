import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store'
import { registerComponents } from './components/module'
import { Quasar } from 'quasar'
import quasarIconSet from 'quasar/icon-set/svg-fontawesome-v6'
import '@quasar/extras/fontawesome-v6/fontawesome-v6.css'
import 'quasar/src/css/index.sass'

const app = createApp(App)

registerComponents(app)

app
    .use(store)
    .use(router)
    .use(Quasar, {
        plugins: {},
        iconSet: quasarIconSet,
    })
    .mount('#app')
