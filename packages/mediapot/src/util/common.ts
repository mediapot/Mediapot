export function isNotNil<T>(item: T | undefined): item is T {
  return item != null
}
