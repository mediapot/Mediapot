import { MovieItem } from '@/store/state'

export const enum TitleOrder {
  None,
  Asc,
  Desc,
}

export function sortByTitle(list: MovieItem[], order: TitleOrder): MovieItem[] {
  switch (order) {
    case TitleOrder.None:
      return list
    case TitleOrder.Asc:
      return list.slice(0).sort((a, b) => (a.title ?? '').localeCompare(b.title ?? ''))
    case TitleOrder.Desc:
      return list.slice(0).sort((a, b) => (b.title ?? '').localeCompare(a.title ?? ''))
  }
}
