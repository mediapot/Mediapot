export type RequiredId<T extends OptionalId<unknown>> = T & Required<Pick<T, 'id'>>
interface OptionalId<T> {
  id?: T
}

export function hasId<T extends OptionalId<unknown>>(item: T): item is RequiredId<T> {
  return item.id != null
}
