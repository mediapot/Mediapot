import { globalConfiguration } from './base'
import { AxiosResponse } from 'axios'
import { MovieDetails, MoviesApiFp } from '@mediapot/tmdb-api-axios'

const appendToResponse = [
  'alternative_titles',
  'changes',
  'credits',
  'external_ids',
  'images',
  'keywords',
  'lists',
  'recommendations',
  'release_dates',
  'reviews',
  'similar',
  'translations',
  'videos',
].join(',')

export async function getMovie(movieId: number): Promise<AxiosResponse<MovieDetails>> {
  const x = await MoviesApiFp(globalConfiguration()).getMovieDetails(movieId, undefined, undefined, appendToResponse)
  return x()
}
