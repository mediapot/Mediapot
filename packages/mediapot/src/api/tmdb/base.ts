import { Configuration } from '@mediapot/tmdb-api-axios'
import { store } from '../../store'

export function globalConfiguration(): Configuration {
  return new Configuration({ apiKey: store.state.settings.apiKey })
}
