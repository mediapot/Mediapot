import { SearchApiFp, SearchMultiResultsPaginated, MoviePaginated, TvPaginated } from '@mediapot/tmdb-api-axios'
import { AxiosResponse } from 'axios'
import { globalConfiguration } from './base'

export const enum SearchType {
  All,
  Movie,
  Tv,
}

export async function search(
  term: string,
  page: number,
  type: SearchType.All,
): Promise<AxiosResponse<SearchMultiResultsPaginated>>
export async function search(term: string, page: number, type: SearchType.Movie): Promise<AxiosResponse<MoviePaginated>>
export async function search(term: string, page: number, type: SearchType.Tv): Promise<AxiosResponse<TvPaginated>>
export async function search(
  term: string,
  page: number,
  type: SearchType,
): Promise<AxiosResponse<SearchMultiResultsPaginated | MoviePaginated | TvPaginated>>
export async function search(
  term: string,
  page: number,
  type: SearchType = SearchType.All,
): Promise<AxiosResponse<SearchMultiResultsPaginated | MoviePaginated | TvPaginated>> {
  switch (type) {
    case SearchType.All:
      return searchAll(term, page)
    case SearchType.Movie:
      return searchMovie(term, page)
    case SearchType.Tv:
      return searchTv(term, page)
  }
}

export async function searchAll(term: string, page: number): Promise<AxiosResponse<SearchMultiResultsPaginated>> {
  const x = await SearchApiFp(globalConfiguration()).getSearchMultiPaginated(term, undefined, page)
  return x()
}

export async function searchMovie(term: string, page: number): Promise<AxiosResponse<MoviePaginated>> {
  const x = await SearchApiFp(globalConfiguration()).getSearchMoviePaginated(
    term,
    undefined,
    undefined,
    undefined,
    page,
  )
  return x()
}

export async function searchTv(term: string, page: number): Promise<AxiosResponse<TvPaginated>> {
  const x = await SearchApiFp(globalConfiguration()).getSearchTvPaginated(term, undefined, undefined, page)
  return x()
}
