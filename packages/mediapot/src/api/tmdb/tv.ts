import { globalConfiguration } from './base'
import { AxiosResponse } from 'axios'
import { TVApiFp, TvDetails } from '@mediapot/tmdb-api-axios'

const appendToResponse = [
  'alternative_titles',
  'changes',
  'credits',
  'external_ids',
  'images',
  'keywords',
  'lists',
  'recommendations',
  'release_dates',
  'reviews',
  'similar',
  'translations',
  'videos',
].join(',')

export async function getTv(tvId: number): Promise<AxiosResponse<TvDetails>> {
  const x = await TVApiFp(globalConfiguration()).getTvDetails(tvId, undefined, undefined, appendToResponse)
  return x()
}
