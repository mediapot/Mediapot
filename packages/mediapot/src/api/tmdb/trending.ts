import { SearchMultiResultsPaginated, TrendingApiFp } from '@mediapot/tmdb-api-axios'
import { AxiosResponse } from 'axios'
import { globalConfiguration } from './base'

export async function getTrending(): Promise<AxiosResponse<SearchMultiResultsPaginated>> {
  const x = await TrendingApiFp(globalConfiguration()).getTrendingPaginated('all', 'week')
  return x()
}
