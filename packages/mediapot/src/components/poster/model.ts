export type ItemVariants = Movie | Tv | Unknown

interface Movie {
  type: 'movie'
  id: number
}

interface Tv {
  type: 'tv'
  id: number
}

interface Unknown {
  type: 'unknown'
  id: number
}
