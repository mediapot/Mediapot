import { ItemVariants } from '@/components/poster/model'

interface Input {
  media_type: 'movie' | 'tv' | string
  id: number
}

export function toVariant(item: Input): ItemVariants {
  switch (item.media_type) {
    case 'movie':
      return {
        type: 'movie',
        id: item.id,
      }
    case 'tv':
      return {
        type: 'tv',
        id: item.id,
      }
    default:
      return {
        type: 'unknown',
        id: item.id,
      }
  }
}
