import { App } from 'vue'
import Modal from './base/Modal.vue'
import Button from './base/Button.vue'
import MoviePoster from './poster/MoviePoster.vue'
import TvPoster from './poster/TvPoster.vue'
import PosterImage from './poster/PosterImage.vue'
import TvEdit from './modal/TvEdit.vue'
import MovieEdit from './modal/MovieEdit.vue'
import Poster from './poster/Poster.vue'

export function registerComponents(app: App): void {
  app
    .component('mp-modal', Modal)
    .component('mp-button', Button)
    .component('tv-edit', TvEdit)
    .component('movie-edit', MovieEdit)
    .component('poster-image', PosterImage)
    .component('tv-poster', TvPoster)
    .component('movie-poster', MoviePoster)
    .component('poster', Poster)
}
