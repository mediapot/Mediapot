import { ActionContext, ActionTree } from 'vuex'
import { MovieItem, TvItem, State } from './state'
import { Mutations, MutationTypes } from './mutations'

export const enum ActionTypes {
  SetApiKey = 'SetApiKey',
  AddMovie = 'AddMovie',
  RemoveMovie = 'RemoveMovie',
  AddTv = 'AddTv',
  RemoveTv = 'RemoveTv',
}

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(key: K, payload: Parameters<Mutations[K]>[1]): ReturnType<Mutations[K]>
} & Omit<ActionContext<State, State>, 'commit'>

export interface Actions {
  [ActionTypes.SetApiKey](context: AugmentedActionContext, key: string): void
  [ActionTypes.AddMovie](context: AugmentedActionContext, data: MovieItem): void
  [ActionTypes.RemoveMovie](context: AugmentedActionContext, id: number): void
  [ActionTypes.AddTv](context: AugmentedActionContext, data: TvItem): void
  [ActionTypes.RemoveTv](context: AugmentedActionContext, id: number): void
}

export const actions: ActionTree<State, State> & Actions = {
  [ActionTypes.SetApiKey]({ commit }, key: string) {
    commit(MutationTypes.SetApiKey, key)
  },
  [ActionTypes.AddMovie]({ commit }, item: MovieItem) {
    commit(MutationTypes.AddMovie, item)
  },
  [ActionTypes.RemoveMovie]({ commit }, id: number) {
    commit(MutationTypes.RemoveMovie, id)
  },
  [ActionTypes.AddTv]({ commit }, item: TvItem) {
    commit(MutationTypes.AddTv, item)
  },
  [ActionTypes.RemoveTv]({ commit }, id: number) {
    commit(MutationTypes.RemoveTv, id)
  },
}
