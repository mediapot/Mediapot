import { State } from '@/store/state'
import { GetterTree } from 'vuex'

export type Getters = {
  apiKey(state: State): string
}

export const getters: GetterTree<State, State> & Getters = {
  apiKey(state: State): string {
    return state.settings.apiKey
  },
}
