export interface State {
  settings: {
    apiKey: string
  }
  list: {
    movie: Partial<Record<number, MovieItem>>
    tv: Partial<Record<number, TvItem>>
  }
}

export interface MovieItem {
  id: number
  score?: number
  status: Status
  notes: string
  title?: string
}

export interface TvItem {
  id: number
  score?: number
  status: Status
  notes: string
  progress?: number
  title?: string
}

export const enum Status {
  None,
  InProgress,
  Finished,
  OnHold,
  Dropped,
  Planned,
}

export const state: State = {
  settings: {
    apiKey: '',
  },
  list: {
    movie: {},
    tv: {},
  },
}
