import { MovieItem, TvItem, State } from '@/store/state'
import { MutationTree } from 'vuex'

export const enum MutationTypes {
  SetApiKey = 'SetApiKey',
  AddMovie = 'AddMovie',
  RemoveMovie = 'RemoveMovie',
  AddTv = 'AddTv',
  RemoveTv = 'RemoveTv',
}

export type Mutations<S = State> = {
  [MutationTypes.SetApiKey](state: S, key: string): void
  [MutationTypes.AddMovie](state: S, item: MovieItem): void
  [MutationTypes.RemoveMovie](state: S, id: number): void
  [MutationTypes.AddTv](state: S, item: TvItem): void
  [MutationTypes.RemoveTv](state: S, id: number): void
}

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.SetApiKey](state: State, key: string): void {
    state.settings.apiKey = key
  },
  [MutationTypes.AddMovie](state: State, item: MovieItem): void {
    state.list.movie[item.id] = item
  },
  [MutationTypes.RemoveMovie](state: State, id: number): void {
    state.list.movie[id] = undefined
  },
  [MutationTypes.AddTv](state: State, item: TvItem): void {
    state.list.tv[item.id] = item
  },
  [MutationTypes.RemoveTv](state: State, id: number): void {
    state.list.tv[id] = undefined
  },
}
