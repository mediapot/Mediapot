import { CommitOptions, createStore, DispatchOptions, Store as VuexStore } from 'vuex'
import VuexPersistence from 'vuex-persist'
import localForage from 'localforage'
import { State, state } from './state'
import { Actions, actions } from './actions'
import { Getters, getters } from './getters'
import { Mutations, mutations } from './mutations'

const vuexLocal = new VuexPersistence({
  // @ts-ignore
  storage: localForage,
  asyncStorage: true,
})

export const store = createStore({
  plugins: [vuexLocal.plugin],
  state,
  mutations,
  actions,
  getters,
})

export type Store = Omit<VuexStore<State>, 'getters' | 'commit' | 'dispatch'> & {
  commit<K extends keyof Mutations, P extends Parameters<Mutations[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions,
  ): ReturnType<Mutations[K]>
} & {
  dispatch<K extends keyof Actions>(
    key: K,
    payload: Parameters<Actions[K]>[1],
    options?: DispatchOptions,
  ): ReturnType<Actions[K]>
} & {
  getters: {
    [K in keyof Getters]: ReturnType<Getters[K]>
  }
}

export function useStore(): Store {
  return store
}
